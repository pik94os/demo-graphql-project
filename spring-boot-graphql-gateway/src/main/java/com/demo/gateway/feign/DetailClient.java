package com.demo.gateway.feign;

import com.demo.gateway.model.graph.DetailItem;
import com.demo.gateway.model.graph.LimitItem;
import com.demo.gateway.model.graph.LimitType;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(
        name = "service-two-detail-api",
        url = "http://127.0.0.1:7771"
)
@RequestMapping("/details")
public interface DetailClient {
    @GetMapping(params = "limitId")
    List<DetailItem> getDetailsByLimitId(@RequestParam Integer limitId);

    @GetMapping(params = "limitIdList")
    List<DetailItem> getDetailsByLimitIdList(@RequestParam List<Integer> limitIdList);
}
