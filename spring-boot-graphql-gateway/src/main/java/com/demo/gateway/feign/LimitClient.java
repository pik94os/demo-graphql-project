package com.demo.gateway.feign;

import com.demo.gateway.model.graph.LimitItem;
import com.demo.gateway.model.graph.LimitType;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient(
        name = "service-one-limit-api",
        url = "http://127.0.0.1:7770"
)
@RequestMapping("/limits")
public interface LimitClient {
    @GetMapping
    List<LimitItem> getLimits();

    @GetMapping("/{limitType}")
    List<LimitItem> getLimitsByLimitType(@PathVariable("limitType") LimitType limitType);
}
