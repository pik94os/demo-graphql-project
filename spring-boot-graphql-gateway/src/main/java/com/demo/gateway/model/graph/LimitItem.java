package com.demo.gateway.model.graph;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

@Data
@Accessors(chain = true)
public class LimitItem {
    private Integer id;
    private String clientName;
    private String limitType;
    private BigDecimal sum;
    private List<DetailItem> details;
}
