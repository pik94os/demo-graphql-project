package com.demo.gateway.repository;

import com.demo.gateway.model.graph.DetailItem;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FakeDetailRepository {
    public List<DetailItem> getDetails(List<Long> paramsIds){
        return List.of(
                new DetailItem().setId(2).setInfo("info 2").setLimitId(3),
                new DetailItem().setId(3).setInfo("info 3").setLimitId(2),
                new DetailItem().setId(1).setInfo("info 1").setLimitId(1),
                new DetailItem().setId(4).setInfo("info 11").setLimitId(1)
        );
    }
}
