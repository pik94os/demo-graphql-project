package com.demo.gateway.repository;

import com.demo.gateway.model.graph.LimitItem;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FakeLimitRepository {
    public List<LimitItem> getLimits(){
        return List.of(
                new LimitItem().setId(1).setClientName("cc 1"),
                new LimitItem().setId(2).setClientName("cc 2"),
                new LimitItem().setId(3).setClientName("cc 3"),
                new LimitItem().setId(4).setClientName("c 4")
        );
    }
}
