package com.demo.gateway.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.demo.gateway.model.graph.DetailItem;
import com.demo.gateway.model.graph.LimitItem;
import graphql.kickstart.execution.context.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import org.dataloader.DataLoader;
import org.dataloader.DataLoaderRegistry;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static com.demo.gateway.resolver.loader.LimitGraphQLContextBuilder.DL_NAME;

/**
 * @see LimitContainerResolver parent
 */

@Component
@SuppressWarnings("unused")
public class LimitItemResolver implements GraphQLResolver<LimitItem> {

    public CompletableFuture<List<DetailItem>> details(LimitItem limitItem, DataFetchingEnvironment dfe){
        Optional<DataLoaderRegistry> dataLoaderRegistry = ((GraphQLContext) dfe.getContext())
                .getDataLoaderRegistry();

        if (dataLoaderRegistry.isPresent()){
            DataLoader<Integer, List<DetailItem>> detailDataLoader =
                    dataLoaderRegistry.get().getDataLoader(DL_NAME);

            return detailDataLoader.load(limitItem.getId());
        }

        throw new RuntimeException(String.format("Dataloader %s не найден.", DL_NAME));
    }

//    public List<DetailItem> details(LimitItem limitItem){
//        return null;
//    }
}
