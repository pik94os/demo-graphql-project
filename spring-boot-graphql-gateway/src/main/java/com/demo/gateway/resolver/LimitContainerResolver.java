package com.demo.gateway.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.demo.gateway.feign.LimitClient;
import com.demo.gateway.model.graph.LimitContainer;
import com.demo.gateway.model.graph.LimitItem;
import com.demo.gateway.model.graph.LimitType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @see LimitQueryRootResolver parent
 * @see LimitItemResolver child
 */


@Component
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class LimitContainerResolver implements GraphQLResolver<LimitContainer> {
//    private final FakeLimitRepository fakeLimitRepository;
    private final LimitClient limitClient;

    public List<LimitItem> report(LimitContainer limitContainer, LimitType limitType){
        return limitClient.getLimits();
    }

    public String exportFile(LimitContainer limitContainer) {
        return "base64 string";
    }
}
