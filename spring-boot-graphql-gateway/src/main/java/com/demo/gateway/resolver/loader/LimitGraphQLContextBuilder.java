package com.demo.gateway.resolver.loader;

import com.demo.gateway.feign.DetailClient;
import com.demo.gateway.model.graph.DetailItem;
import graphql.kickstart.execution.context.DefaultGraphQLContext;
import graphql.kickstart.execution.context.GraphQLContext;
import graphql.servlet.context.DefaultGraphQLServletContext;
import graphql.servlet.context.DefaultGraphQLWebSocketContext;
import graphql.servlet.context.GraphQLServletContextBuilder;
import lombok.RequiredArgsConstructor;
import org.dataloader.DataLoader;
import org.dataloader.DataLoaderRegistry;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Session;
import javax.websocket.server.HandshakeRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class LimitGraphQLContextBuilder implements GraphQLServletContextBuilder {
    public static final String DL_NAME = "detailDataLoader";

//    private final FakeDetailRepository fakeDetailRepository;
    private final DetailClient detailClient;


    @Override
    public GraphQLContext build(HttpServletRequest req, HttpServletResponse response) {
        return DefaultGraphQLServletContext.createServletContext(buildDataLoaderRegistry(), null).with(req).with(response)
                .build();
    }

    @Override
    public GraphQLContext build() {
        return new DefaultGraphQLContext(buildDataLoaderRegistry(), null);
    }

    @Override
    public GraphQLContext build(Session session, HandshakeRequest request) {
        return DefaultGraphQLWebSocketContext.createWebSocketContext(buildDataLoaderRegistry(), null).with(session)
                .with(request).build();
    }

    private DataLoaderRegistry buildDataLoaderRegistry() {
        DataLoaderRegistry dataLoaderRegistry = new DataLoaderRegistry();
        dataLoaderRegistry.register(DL_NAME,
                new DataLoader<Integer, List<DetailItem>>(limitId ->
                        CompletableFuture.supplyAsync(() -> {
                                    List<DetailItem> detailItems = detailClient.getDetailsByLimitIdList(limitId);
                                    Map<Integer, List<DetailItem>> map = new HashMap<>();
                                    detailItems.forEach(d -> {
                                        map.put(d.getLimitId(), getDetails(d.getLimitId(), detailItems));
                                    });
                                    return limitId.stream().map(map::get).collect(Collectors.toList());
                                }
                        )));

        return dataLoaderRegistry;
    }

    private List<DetailItem> getDetails(int limitId, List<DetailItem> detailItems) {
        return detailItems.stream()
                .filter(d -> d.getLimitId().equals(limitId))
                .collect(Collectors.toList());
    }
}
