package com.demo.gateway.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.demo.gateway.model.graph.LimitContainer;
import org.springframework.stereotype.Component;

/**
 * @see LimitContainerResolver child
 */

@Component
@SuppressWarnings("unused")
public class LimitQueryRootResolver implements GraphQLQueryResolver {
    public LimitContainer limit(){
        return new LimitContainer();
    }
}
