package com.demo.serviceone.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "limits")
public class Limit {
    @Id
    private Integer id;

    @Column(name = "client_name")
    private String clientName;

    @Column(name = "limit_type")
    @Enumerated(EnumType.STRING)
    private LimitType limitType;

    @Column(name = "sum")
    private BigDecimal sum;
}
