package com.demo.serviceone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceOne {

    public static void main(String[] args) {
        SpringApplication.run(ServiceOne.class, args);
    }

}
