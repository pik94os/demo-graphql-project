package com.demo.serviceone.repository;

import com.demo.serviceone.model.Limit;
import com.demo.serviceone.model.LimitType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LimitRepository extends JpaRepository<Limit, Integer> {
    List<Limit> findAllByLimitType(LimitType limitType);
}
