package com.demo.serviceone.controller;

import com.demo.serviceone.model.Limit;
import com.demo.serviceone.model.LimitType;
import com.demo.serviceone.repository.LimitRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/limits")
@RequiredArgsConstructor
public class LimitController {
    private final LimitRepository limitRepository;

    @GetMapping
    public List<Limit> getLimits(){
        return limitRepository.findAll();
    }


    @GetMapping("/{limitType}")
    public List<Limit> getLimitsByLimitType(@PathVariable("limitType") LimitType limitType){
        return limitRepository.findAllByLimitType(limitType);
    }
}
