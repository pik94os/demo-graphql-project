drop table if exists limits;

create table limits (
    id int primary key,
    client_name varchar(255) not null,
    limit_type varchar(50) not null,
    sum numeric(18, 2) not null
);

insert into limits (id, client_name, limit_type, sum)
values
       ( 1, 'client 1', 'TYPE_ONE', 100000),
       ( 2, 'client 2', 'TYPE_ONE', 200000),
       ( 3, 'client 3', 'TYPE_TWO', 300000),
       ( 4, 'client 4', 'TYPE_TWO', 400000),
       ( 5, 'client 5', 'TYPE_TWO', 500000),
       ( 6, 'client 6', 'TYPE_TWO', 600000),
       ( 7, 'client 7', 'TYPE_ONE', 700000);