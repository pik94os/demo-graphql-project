package com.demo.servicetwo.controller;

import com.demo.servicetwo.model.Detail;
import com.demo.servicetwo.repository.DetailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/details")
@RequiredArgsConstructor
public class LimitController {
    private final DetailRepository detailRepository;

    @GetMapping(params = "limitId")
    public List<Detail> getDetailsByLimitId(@RequestParam Integer limitId ){
        return detailRepository.findAllByLimitId(limitId);
    }

    @GetMapping(params = "limitIdList")
    public List<Detail> getDetailsByLimitIdList(@RequestParam List<Integer> limitIdList){
        return detailRepository.findAllByLimitIdIn(limitIdList);
    }
}
