package com.demo.servicetwo.repository;

import com.demo.servicetwo.model.Detail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DetailRepository extends JpaRepository<Detail, Integer> {
    List<Detail> findAllByLimitId(Integer limitId);
    List<Detail> findAllByLimitIdIn(List<Integer> limitIdList);
}
