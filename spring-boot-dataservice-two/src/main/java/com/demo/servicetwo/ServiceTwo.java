package com.demo.servicetwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceTwo {

    public static void main(String[] args) {
        SpringApplication.run(ServiceTwo.class, args);
    }

}
