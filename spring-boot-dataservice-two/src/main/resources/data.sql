drop table if exists details;

create table details (
    id int primary key,
    limit_id int not null,
    info varchar(255) not null
);

insert into details (id, limit_id, info)
values
       ( 1, 1, 'info 1'),
       ( 2, 2, 'info 2'),
       ( 3, 2, 'info 3'),
       ( 4, 3, 'info 4'),
       ( 5, 3, 'info 5'),
       ( 6, 5, 'info 6');
